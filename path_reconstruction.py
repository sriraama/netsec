import re
flows = []
class flowRecord:
	def __init__(self,record,dpid):
		d = []
		e = []
		a = '10.0.0.1/32'
		self.entry = {}
		for y in record:
			d += y.split(",")
		for x in d:
			e.append(x.split(":"))
		for x in range(0,len(e)-1):
			for t in range(0,len(e[x])-1):
				e[x][t] = e[x][t].strip()
				e[x][t] = re.sub("[' ]","",e[x][t])
				e[x][t] = e[x][t].strip()
		for x in e:
			if len(x)  == 2:
				if (x[1] != ' ')  :
					x[1] = x[1].strip()
					x[0] = x[0].strip()
					x[1] = re.sub("[']","",x[1])
					x[0] = re.sub("[']","",x[0])
					self.entry[x[0]] = x[1]
		self.dpid = dpid
		
def parseFlowRecords():			
	global flows
	fd = open("flow.log","r")
	entries = fd.readlines()
	for entry in entries:
		entry = entry.strip()
		out = entry.split(":",1)
		entry = out[1]
		regex = re.compile("[{}]")
		t = re.split(regex,entry)
		for x in range(len(t)-1,-1,-1):
			if t[x] == '':
				del t[x]
		f = flowRecord(t,int(out[0]))
		flows.append(f)
	fd.close()			

def findMatchingRecords(a,b):
	global flows
	subsetFlows = []
	for t in flows:
		if t.entry['nw_dst'] == a:
			if t.entry['dl_type'] == 'IP':
				if t.entry['nw_src'] == b:
					subsetFlows.append(t)
	return subsetFlows

def getLinks(links,a):
	result = []
	for link in links:
		if a in link:
			result.append(link)
	return result

def removeLinks(links,link):
	for x in range(len(links)-1,-1,-1):
		if links[x] == link:
			del links[x]
	return links
	
def connectLinks(links):	
	route = []
	t = links[0]
	del links[0]
	links = removeLinks(links,t)
	for x in t:
		route.append(x)

	while ((route[0] != 'eh') | (route[len(route)-1] != 'eh')) & (len(links)>0):
		a = route[0]
		b = route[len(route)-1]
		if(a != 'eh'):
			t = getLinks(links,a)
			if len(t) > 0:
				for x in t:
					links = removeLinks(links,x)
				for x in t:
					for a in x:
						if a not in route:
							route.insert(0,a)
						else:
							if a == 'eh':
								route.insert(0,a)
				
		if(b != 'eh'):
			t = getLinks(links,b)
			if len(t) > 0:
				for x in t:
					links = removeLinks(links,x)
				for x in t:
					for a in x:
						if a not in route:
							route.append(a)
						else:
							if a == 'eh':
								route.append(a)
	print route

def getTrafficSources(a):
	global flows
	sources = []
	for t in flows:
		if t.entry['nw_dst'] == a:
			if t.entry['dl_type'] == 'IP':
				if t.entry['nw_src'] not in sources:
					sources.append(t.entry['nw_src'])
	return sources
	
adjList = [[0,0,0,0,0, 0,0,0,0,0, 0,0,1,0,0, 0],[0,0,0,0,0, 0,0,0,0,0, 0,0,1,0,0, 0],[0,0,0,0,0, 0,0,0,0,0, 0,0,1,0,0, 0],[0,0,0,0,0, 0,0,0,0,0, 0,0,0,1,0, 0],[0,0,0,0,0, 0,0,0,0,0, 0,0,0,1,0, 0],[0,0,0,0,0, 0,0,0,0,0, 0,0,0,1,0, 0],[0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,1, 0],[0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,1, 0],[0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,1, 0],[0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 1],[0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 1],[0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 1],[2,3,4,0,0, 0,0,0,0,0, 0,0,0,1,0, 0],[0,0,0,3,4, 5,0,0,0,0, 0,0,1,0,0, 2],[0,0,0,0,0, 0,2,3,4,0, 0,0,0,0,0, 1],[0,0,0,0,0, 0,0,0,0,3, 4,5,0,1,2, 0]]	

destn = '10.0.0.1/32'
parseFlowRecords()	
sources = getTrafficSources(destn)
for source in sources:
	subsetFlows = findMatchingRecords(destn,source)
	print destn,source
	q = []
	paths = {}
	for x in subsetFlows:
		paths[x.dpid]=(int(x.entry['in_port']),int(x.entry['port']))
		
	switches = paths.keys()
	links = []
	checked = []
	q.append(switches[0])
	while len(q) > 0:
		x = q.pop()
		checked.append(x)
		adj = adjList[x-1]
		
		for v in paths[x]:
			if v in adj:
				links.append(set([x, (adj.index(v))+1]))
				if (adj.index(v)+1) not in checked:
					q.append((adj.index(v))+1)
					
			else:
				links.append(set([x,"eh"]))
		
	
	connectLinks(links)	
	
