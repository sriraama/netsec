import re
flows = []
class flowRecord:
	def __init__(self,record,dpid):
		d = []
		e = []
		a = '10.0.0.1/32'
		self.entry = {}
		for y in record:
			d += y.split(",")
		for x in d:
			e.append(x.split(":"))
		for x in range(0,len(e)-1):
			for t in range(0,len(e[x])-1):
				e[x][t] = e[x][t].strip()
				e[x][t] = re.sub("[' ]","",e[x][t])
				e[x][t] = e[x][t].strip()
		for x in e:
			if len(x)  == 2:
				if (x[1] != ' ')  :
					x[1] = x[1].strip()
					x[0] = x[0].strip()
					x[1] = re.sub("[']","",x[1])
					x[0] = re.sub("[']","",x[0])
					self.entry[x[0]] = x[1]
		self.dpid = dpid
		
def parseFlowRecords():			
	global flows
	fd = open("flow.log","r")
	entries = fd.readlines()
	for entry in entries:
		entry = entry.strip()
		out = entry.split(":",1)
		entry = out[1]
		regex = re.compile("[{}]")
		t = re.split(regex,entry)
		for x in range(len(t)-1,-1,-1):
			if t[x] == '':
				del t[x]
		f = flowRecord(t,int(out[0]))
		flows.append(f)
	fd.close()			

def findMatchingRecords(a,b):
	global flows
	subsetFlows = []
	for t in flows:
		if t.entry['nw_dst'] == a:
			if t.entry['dl_type'] == 'IP':
				if t.entry['nw_src'] == b:
					subsetFlows.append(t)
	return subsetFlows
def flowsSwitch():
	global flows
	switches = []
	subsetFlows = []
	for t in flows:
		if t.dpid not in switches:
			switches.append(t.dpid)
	
	for n in switches:
		subsetFlows.append([])
		for t in flows:
			if n == t.dpid:
				if len(t.entry) > 0:
					subsetFlows[len(subsetFlows)-1].append(t.entry)
			
	return switches,subsetFlows

parseFlowRecords()
counter= {}
switches,subflows = flowsSwitch()		
for x in range(0,len(switches)-1):
	switch = switches[x]
	sflows = subflows[x]
	count = 0
	for y in sflows:
		count += int(y['byte_count'])
	counter[switch] = count

print counter
